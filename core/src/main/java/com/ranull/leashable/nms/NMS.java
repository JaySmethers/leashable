package com.ranull.leashable.nms;

import org.bukkit.entity.Entity;

public interface NMS {
    void leash(Entity entity, Entity holder);

    void unleash(Entity entity);

    boolean canHaveLeash(Entity entity);

    boolean canBeLeashed(Entity entity);

    Entity getHolder(Entity entity);
}
